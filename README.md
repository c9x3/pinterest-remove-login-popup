# [C9x3] Pinterest Remove Login Popup

Attempts to redirect the Pinterest url to its url without the login popup. You MAY have to reload the site from time to time!

# Licensing and donation information:

https://c9x3.neocities.org/

# How to run this Userscript

- Get a userscript runner. For examlpe, Violentmonkey. 
- Create a new script and copy and paste the contents into the new userscript! 
- Save that script, exit and test the script out. 
