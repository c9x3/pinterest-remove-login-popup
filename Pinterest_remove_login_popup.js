// ==UserScript==
// @name [C9x3] Pinterest Remove Login Popup
// @version 1.8
// @description Attempts to redirect the Pinterest url to its url without the login popup.
// @match       *://*/*
// @grant none
// ProjectHomepage  https://c9x3.neocities.org/
// @downloadURL  		https://gitlab.com/c9x3/pinterest-remove-login-popup/-/raw/main/Pinterest_remove_login_popup.js
// @updateURL    		https://gitlab.com/c9x3/pinterest-remove-login-popup/-/raw/main/Pinterest_remove_login_popup.js
// ==/UserScript==

// Make a log in the console when the script starts.

console.log('Pinterest remove login popup script has started...')

//
//
//
//
//

// Don’t run in frames.

if (window.top !== window.self)

  return;

var currentURL = location.href;

//
//
//
//
//

// If webpage url contains, "/?d=t&" then automatically, remove the, "/?d=t&" and reload the webpage.

if (currentURL.match("/?d=t&")) {

	location.href = location.href.replace("/?d=t&", "");

};

//
//
//
//
//

// Set timeout for, "/?d=t&." (Example, 20*1000ms = 20 seconds.) I've made the script auto-refresh after 1 second.

if (currentURL.match("/?d=t&")) {

  setTimeout(function(){ location.reload(); }, 1*1000);

};

//
//
//
//
//

// If webpage url contains, "mt=login" then automatically, remove the, "mt=login" and reload the webpage.

if (currentURL.match("mt=login")) {

	location.href = location.href.replace("mt=login", "");

};

//
//
//
//
//

// Set timeout for, "mt=login." (Example, 20*1000ms = 20 seconds.) I've made the script auto-refresh after 1 second.

if (currentURL.match("mt=login")) {

  setTimeout(function(){ location.reload(); }, 1*1000);

};

//
//
//
//
//

// end of script.

// Helpful links:
//
// https://gist.github.com/thefloodshark/b24636d8129dd28ef7a2cb7d5a8a1dce
// https://greasyfork.org/en/discussions/requests/55817-replace-string-in-an-url
// https://www.pinterest.com/pin/89720217561899723/?d=t&mt=login (try this link to test the script.)
//
//
